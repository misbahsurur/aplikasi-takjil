package marsel.oky.appx0b

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.menu_takjil.*
import org.json.JSONArray
import org.json.JSONObject

class menuActivity : AppCompatActivity(), View.OnClickListener {


    lateinit var menuAdapter: AdapterMenu
    lateinit var mediaHelper : MediaHelper
    var daftarMenu = mutableListOf<HashMap<String, String>>()
    //val url = "http://192.168.43.221/aplikasi-takjil-web/"
    val url1 = "http://192.168.43.221/aplikasi-takjil-web/show_data.php"
    val url3 = "http://192.168.43.221/aplikasi-takjil-web/crud_data.php"
    var imStr = ""
    var namafile = ""
    var fileUri = Uri.parse("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu_takjil)
        menuAdapter = AdapterMenu(daftarMenu,this)
        mediaHelper = MediaHelper(this)
        ls_menu.layoutManager = LinearLayoutManager(this)
        ls_menu.adapter = menuAdapter

        imageView2.setOnClickListener(this)
        bt_add.setOnClickListener(this)
        bt_edit.setOnClickListener(this)
        bt_del.setOnClickListener(this)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onStart() {
        super.onStart()
        show()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imageView2->{
                requestPermission()
                daftarMenu.clear()
            }
            R.id.bt_add->{
                crud("insert")
                daftarMenu.clear()
            }

            R.id.bt_edit->{
                crud("update")
                daftarMenu.clear()
            }

            R.id.bt_del->{
                crud("delete")
                daftarMenu.clear()
            }
        }
    }

    fun requestPermission() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        fileUri = mediaHelper.getOutputMediaFileuri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent,mediaHelper.getRcCamera())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK)
            if(requestCode == mediaHelper.getRcCamera()){
                imStr = mediaHelper.getBitmapToString(fileUri,imageView2)
                namafile = mediaHelper.getMyFileName()
            }
    }

    fun show(){
        val request = StringRequest(Request.Method.POST,url1,Response.Listener { response ->
                daftarMenu.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 ..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mn = HashMap<String,String>()
                    mn.put("id",jsonObject.getString("id"))
                    mn.put("nama",jsonObject.getString("nama"))
                    mn.put("harga",jsonObject.getString("harga"))
                    mn.put("url",jsonObject.getString("url"))
                    daftarMenu.add(mn)
                }
                menuAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun crud(mode: String) {
        val request = object : StringRequest(
            Method.POST, url3,
            Response.Listener { response ->
                val jsonobject = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")) {
                    Toast.makeText(this, "Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    show()
                } else {
                    Toast.makeText(this, "Operasi Gagal ", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                when (mode) {
                    "insert" -> {
                        hm.put("mode", "insert")
                        hm.put("id", tx_id.text.toString())
                        hm.put("nama", tx_nama.text.toString())
                        hm.put("images", imStr)
                        hm.put("file", namafile)
                        hm.put("harga", tx_harga.text.toString())
                    }
                    "update" -> {
                        hm.put("mode", "update")
                        hm.put("id", tx_id.text.toString())
                        hm.put("nama", tx_nama.text.toString())
                        hm.put("images", imStr)
                        hm.put("file", namafile)
                        hm.put("harga", tx_harga.text.toString())
                    }
                    "delete" -> {
                        hm.put("mode", "delete")
                        hm.put("id", tx_id.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

}