package marsel.oky.appx0b

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.menu_takjil.*

class AdapterMenu(val dataMenu: List<HashMap<String,String>>, val menuActivity: menuActivity): RecyclerView.Adapter<AdapterMenu.HolderDataMenu>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterMenu.HolderDataMenu {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.row_takjil,parent,false)
        return HolderDataMenu(v)
    }

    override fun getItemCount(): Int {
        return dataMenu.size
    }

    override fun onBindViewHolder(holder: AdapterMenu.HolderDataMenu, position: Int) {
        val data =dataMenu.get(position)
        holder.tx_id.setText(data.get("id"))
        holder.tx_nama.setText(data.get("nama"))
        holder.tx_harga.setText(data.get("harga"))
        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))
        holder.layouts.setOnClickListener(View.OnClickListener{
            menuActivity.tx_id.setText(data.get("id"))
            menuActivity.tx_nama.setText(data.get("nama"))
            menuActivity.tx_harga.setText(data.get("harga"))
            Picasso.get().load(data.get("url")).into(menuActivity.imageView2)
        })
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.images);
    }

    class HolderDataMenu(v : View) :  RecyclerView.ViewHolder(v){
        val tx_id = v.findViewById<TextView>(R.id.ed_id)
        val tx_nama = v.findViewById<TextView>(R.id.ed_nama)
        val tx_harga = v.findViewById<TextView>(R.id.ed_harga)
        val images = v.findViewById<ImageView>(R.id.imageView3)
        val layouts = v.findViewById<ConstraintLayout>(R.id.rowMenu)
    }
}