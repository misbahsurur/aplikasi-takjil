package marsel.oky.appx0b

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bt_data.setOnClickListener(this)
    }



    override fun onClick(v: View?) {
        when(v?.id){
            R.id.bt_data ->{
                var menu = Intent(this, menuActivity::class.java)
                startActivity(menu)
                true
            }
        }
    }

}
